

var myApp = angular.module('myApp',['angularUtils.directives.dirPagination','ui.bootstrap']);



function MyController($scope, $http, $window) {

	$scope.showData = function()
	{
	
	  $scope.currentPage = 1;
	  $scope.pageSize = 10;
	  $scope.groupSearch = {'visibility': 'hidden'};
	  
	  $scope.options = [
	                      {'argument': 'time'},
	                      {'argument': 'phi'},
	                      {'argument': 'theta'}
	                  ];
	  $http({method: 'POST', url: 'json/run.json'}).success(function(data)
			   {
			              $scope.runDetails = data; // response data
			   });

	  $http({method: 'POST', url: 'json/cluster.json'}).success(function(data)
			   {
			              $scope.clusters = data; // response data
			   });
	}
	

	
	/*$scope.arguments= [{"imgSrc" : "1.000000/-150/-30.png"},
	                   {"imgSrc" : "1.000000/-150/-60.png"},
	                   {"imgSrc" :"1.000000/-180/-30.png"}, 
	                	   {"imgSrc" :"1.000000/-180/-60.png"}, 
	                	   {"imgSrc" :"1.000000/-180/0.png"},
	                	   {"imgSrc" : "1.000000/-30/-150.png"},
	                	   {"imgSrc" :"1.000000/0/-120.png"}, 
	                	   {"imgSrc" :"1.000000/0/-150.png"}, 
	                	   {"imgSrc" :"1.000000/0/-180.png"},
	                	   {"imgSrc" :"1.000000/150/-30.png"},
	                	   {"imgSrc" :"1.000000/150/-60.png"}, 
	                	   {"imgSrc" :"1.000000/30/-120.png"}, 
	                	   {"imgSrc" :"1.000000/30/-150.png"}];*/
	
	$scope.pageChangeHandler = function(num) {
	      console.log('images page changed to ' + num);
	  };
	  
	  
		$scope.clearSearch = function() {
			 $scope.arguments = '';
			 $scope.cinemaSearch='';
			  $scope.groupSearch = {'visibility': 'hidden'};	
			  $scope.paginationTab = {'visibility': 'hidden'};		
		  };
		  
	  $scope.doSearch = function() {
			$http.get('http://localhost:8080/CinemaWeb/rest/SearchCinema/'+$scope.cinemaSearch).
	      success(function(data) {
	          $scope.message = data;  
	          if($scope.message=='Null')
	        	  $scope.message = 'Invalid Error';
	          else
	        	  $scope.arguments = data;
	          $scope.groupSearch = {'visibility': 'visible'};	
	          $scope.paginationTab = {'visibility': 'visible'};	
	            });
		  };
		  
		  $scope.setCurrentPageTo = function() {
			  $scope.pageChangeHandler($scope.goToPageNumber);
		  };
		  


	$scope.doSpecificSearch = function(specificQuery) {
		$http.get('http://localhost:8080/CinemaWeb/rest/SearchCinema/'+specificQuery+' and '+$scope.cinemaSearch).
	  success(function(data) {
	      
	    	  $scope.arguments = data;
	          $scope.groupSearch = {'visibility': 'visible'};	
	          $scope.paginationTab = {'visibility': 'visible'};
	          
	          if($scope.cinemaSearch == "time=10")
	        	  $scope.clusters=[];
	        });
	  };
	  
	  
	  $scope.pageChangeHandler = function(num) {
		    console.log('going to page ' + num);
		  };
}

myApp.controller('MyController', MyController);
myApp.controller('PostsCtrlAjax', PostsCtrlAjax);




