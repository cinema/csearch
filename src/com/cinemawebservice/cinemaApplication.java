//===========================================================================
 /*
    This file is part of the Cinema Web Interface Code.
    Copyright (C) by Los Alamos National Lab.
    Author:    Uzma Shaikh
 //===========================================================================*/


package com.cinemawebservice;
   import org.glassfish.jersey.server.ResourceConfig;


   public class cinemaApplication extends ResourceConfig {
      public cinemaApplication() {
          packages("com.cinemawebservice");
    }
   }